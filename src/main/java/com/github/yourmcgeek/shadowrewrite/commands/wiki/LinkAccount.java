package com.github.yourmcgeek.shadowrewrite.commands.wiki;

import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.util.List;

@Command(label = {"link", "accounts", "linkaccount"}, usage = "link", description = "Displays how to link your account with the forums!")
public class LinkAccount {

    @Execute
    public CommandResult onExecute(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowRewrite main) {
        EmbedBuilder embed = new EmbedBuilder()
                .setTitle("Linking Accounts")
                .setDescription("Use this link to see how to link your accounts with the forums, access <#260579843710779392> and the voice channels\n" + main.getMainConfig().getConfigValue("wiki", "linkingURL").getAsString())
                .setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString()));
        main.getMessenger().sendEmbed(channel, embed.build(), 10);
        return CommandResult.success();
    }
}