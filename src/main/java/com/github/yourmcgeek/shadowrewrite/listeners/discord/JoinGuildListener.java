package com.github.yourmcgeek.shadowrewrite.listeners.discord;

import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.time.OffsetDateTime;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class JoinGuildListener extends ListenerAdapter {

    private ShadowRewrite bot;
    private ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(2);

    public JoinGuildListener(ShadowRewrite bot) {
        this.bot = bot;
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        OffsetDateTime time = event.getMember().getJoinDate();
        scheduledExecutorService.schedule(() -> {
            //if (OffsetDateTime.now().minusMinutes(bot.getMainConfig().getConfigValue("timedRole", "timeToWait").getAsInt()).isAfter(time)) {
                event.getGuild().getController().addSingleRoleToMember(event.getMember(), event.getGuild().getRoleById(bot.getMainConfig().getConfigValue("timedRole", "roleToAdd").getAsLong())).queue();
                if (bot.getMainConfig().getConfigValue("debug").getAsBoolean())
                    bot.getLogger().info("Added role to member!");
            //}
        }, bot.getMainConfig().getConfigValue("timedRole", "timeToWait").getAsInt(), TimeUnit.MINUTES);
    }
}
