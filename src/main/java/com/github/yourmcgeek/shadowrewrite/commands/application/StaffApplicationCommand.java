package com.github.yourmcgeek.shadowrewrite.commands.application;

import com.github.yourmcgeek.shadowrewrite.EmbedTemplates;
import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import com.github.yourmcgeek.shadowrewrite.utils.Util;
import me.bhop.bjdautilities.ReactionMenu;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.exceptions.ErrorResponseException;
import net.dv8tion.jda.core.utils.tuple.Pair;
import net.dv8tion.jda.webhook.WebhookClient;
import net.dv8tion.jda.webhook.WebhookClientBuilder;
import net.dv8tion.jda.webhook.WebhookMessage;
import net.dv8tion.jda.webhook.WebhookMessageBuilder;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Command(label = "staffapp", description = "Starts the staff application builder in your private messages!", hideInHelp = true)
public class StaffApplicationCommand {

    // TODO Find a better way to send mark answers correct? HashMaps and then for loop on the HashMap before building to set the fields in the correct spots?
    // TODO Because @Bennyboy1695 wants editing ability so bad, persuade him to add it since its unnecessary imo

    private final ExecutorService executorService = Executors.newScheduledThreadPool(2);
    private EmbedBuilder identificationEmbedBuilder = new EmbedBuilder();
    private EmbedBuilder multipleChoiceEmbedBuilder = new EmbedBuilder();
    private EmbedBuilder answersEmbedBuilder = new EmbedBuilder();
    private WebhookMessageBuilder webhookBulder = new WebhookMessageBuilder();
    private Webhook webhook;
    private WebhookClientBuilder clientBuilder;
    private WebhookClient client;
    WebhookMessage webhookMessage;
    ArrayList<String> messageIds = new ArrayList<>();
    private final boolean[] usernameError = {false},
            hasClickedUsername = {false},
            ageError = {false},
            hasClickedAge = {false},
            timezoneError = {false},
            hasClickedTimezone = {false},
            lengthError = {false},
            hasClickedLength = {false},
            modExpError = {false},
            hasClickedModExp = {false},
            activeServerError = {false},
            hasClickedActiveServer = {false},
            cannotConnectError = {false},
            hasClickedCannotConnect = {false},
            miniModdingError = {false},
            hasClickedMiniModding = {false},
            rulesError = {false},
            hasClickedRules = {false},
            mobSpawnersError = {false},
            hasClickedMobSpawners = {false},
            autoMiningError = {false},
            hasClickedAutoMining = {false},
            griefingError = {false},
            hasClickedGriefing = {false},
            friendError = {false},
            hasClickedFriend = {false},
            prioritiesError = {false},
            hasClickedPriorities = {false},
            hostileError = {false},
            hasClickedHostile = {false},
            finishedError = {false},
            hasClickedError = {false},
            sentPmInfo = {false};

    @Execute
    public CommandResult onStaffApplication(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowRewrite main) {
        identificationEmbedBuilder = new EmbedBuilder().setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString()));
        multipleChoiceEmbedBuilder = new EmbedBuilder().setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString()));
        answersEmbedBuilder = new EmbedBuilder().setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString()));
        clientBuilder = new WebhookClientBuilder(main.getMainConfig().getConfigValue("StaffWebhook").getAsString());
        client = clientBuilder.build();
        messageIds = new ArrayList<>();
        usernameError[0] = false;
        hasClickedUsername[0] = false;
        ageError[0] = false;
        hasClickedAge[0] = false;
        timezoneError[0] = false;
        hasClickedTimezone[0] = false;
        lengthError[0] = false;
        hasClickedLength[0] = false;
        modExpError[0] = false;
        hasClickedModExp[0] = false;
        activeServerError[0] = false;
        hasClickedActiveServer[0] = false;
        cannotConnectError[0] = false;
        hasClickedCannotConnect[0] = false;
        miniModdingError[0] = false;
        hasClickedMiniModding[0] = false;
        rulesError[0] = false;
        hasClickedRules[0] = false;
        mobSpawnersError[0] = false;
        hasClickedMobSpawners[0] = false;
        autoMiningError[0] = false;
        hasClickedAutoMining[0] = false;
        griefingError[0] = false;
        hasClickedGriefing[0] = false;
        friendError[0] = false;
        hasClickedFriend[0] = false;
        prioritiesError[0] = false;
        hasClickedPriorities[0] = false;
        hostileError[0] = false;
        hasClickedHostile[0] = false;
        finishedError[0] = false;
        hasClickedError[0] = false;
        sentPmInfo[0] = false;

        ReactionMenu menu = new ReactionMenu.Builder(main.getJDA())
                .setEmbed(new EmbedBuilder()
                        .setColor(Color.CYAN)
                        .setTitle("Staff Application")
                        .setDescription("If you'd like to apply to become part of our staff team and meet our requirements, please react with a \uD83D\uDCDD\n\n**__Requirements__**:\nOwn and use a Microphone\nBe at least 16 years old\nHave played on our servers for at least 1 month")
                        .build())
                .onClick("\uD83D\uDCDD", (reaction, user) -> {
                    user.openPrivateChannel().queue((success) -> {
                        askName(main, success, channel);
                        if (!sentPmInfo[0])
                            executorService.submit(() -> {
                                main.getMessenger().sendEmbed(channel, new EmbedBuilder().setTitle("Creation Started!").setDescription(user.getAsMention() + ": A private message has been sent to you to start the creation process!").addField("Private Message", "https://discordapp.com/channels/@me/" + success.getIdLong(), false).build(), 20);
                            });
                    });
                }).buildAndDisplay(channel);
        menu.destroyIn(20);
        return CommandResult.success();
    }

    private void askName(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    sentPmInfo[0] = true;
                    ArrayList<String> beforeBotMessages = new ArrayList<>();
                    ReactionMenu pmMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                            (usernameError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't specify any username!\nPlease enter your username!").setFooter("Progress: 0%", null).build() : new EmbedBuilder().setTitle("Username").setDescription("Please enter your username").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 0%", null).build()))
                            .onClick("\u2705", (done, user1) -> {
                                if (!hasClickedUsername[0]) {
                                    success.getHistory().retrievePast(5).queue((history) -> {
                                        for (Message messages : history) {
                                            if (messages.getAuthor().isBot())
                                                break;
                                            beforeBotMessages.add(messages.getContentRaw());
                                        }
                                        if (!beforeBotMessages.isEmpty()) {
                                            if (beforeBotMessages.size() == 1) {
                                                String msg = beforeBotMessages.get(0);
                                                Pair pair = Util.getPlayerInfoFromUsername(msg);
                                                if (pair != null) {
                                                    executorService.submit(() -> {
                                                        identificationEmbedBuilder = identificationEmbedBuilder
                                                                .setTitle("**" + pair.getRight().toString().replace("_", "\\_") + "'s** Staff Application", "https://namemc.com/search?q=" + pair.getLeft())
                                                                .setThumbnail("https://mc-heads.net/avatar/" + pair.getLeft().toString() + ".png");
                                                        ReactionMenu identificationEmbed = new ReactionMenu.Builder(main.getJDA())
                                                                .setEmbed(identificationEmbedBuilder.build())
                                                                .buildAndDisplayForPrivateMessage(success);
                                                        messageIds.add(String.valueOf(identificationEmbed.getMessage().getIdLong()));
                                                    });
                                                } else {
                                                    executorService.submit(() -> {
                                                        main.getMessenger().sendEmbed(success, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("The username: `" + msg + "` isn't a valid username! Please type it again making sure you correctly spell it.").build(), 10);
                                                        usernameError[0] = true;
                                                        this.run();
                                                    });
                                                }
                                            }
                                        } else {
                                            for (String usernameMessages : beforeBotMessages) {
                                                Pair pair = Util.getPlayerInfoFromUsername(usernameMessages);
                                                if (pair != null) {
                                                    executorService.submit(() -> {
                                                        identificationEmbedBuilder = identificationEmbedBuilder
                                                                .setTitle("**" + pair.getRight().toString().replace("_", "\\_") + "'s** Staff Application", "https://namemc.com/search?q=" + pair.getLeft())
                                                                .setThumbnail("https://mc-heads.net/avatar/" + pair.getLeft().toString() + ".png");
                                                        ReactionMenu identificationEmbed = new ReactionMenu.Builder(main.getJDA())
                                                                .setEmbed(identificationEmbedBuilder.build())
                                                                .setStartingReactions("\uD83D\uDCDD")
                                                                .buildAndDisplayForPrivateMessage(channel);
                                                        messageIds.add(String.valueOf(identificationEmbed.getMessage().getIdLong()));
                                                    });
                                                } else {
                                                    executorService.submit(() -> {
                                                        main.getMessenger().sendEmbed(success, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("The username: `" + usernameMessages + "` isn't a valid username! Please type it again making sure you correctly spell it.").build(), 10);
                                                        usernameError[0] = true;
                                                        this.run();
                                                    });
                                                }
                                            }
                                        }
                                        if (!usernameError[0]) {
                                            askAge(main, success, channel);
                                            hasClickedUsername[0] = true;
                                        } else {
                                            usernameError[0] = true;
                                            this.run();
                                        }
                                    });
                                }
                            }).buildAndDisplayForPrivateMessage(success);
                } catch (ErrorResponseException e) {
                    sentPmInfo[0] = false;
                    main.getMessenger().sendEmbed(channel, new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You do not have private messages open! Unable to start application process without being able to private message you!").build(), 20);
                }
            }
        });
    }

    private void askAge(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> ageHistory = new ArrayList<>();
                ReactionMenu ageMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (ageError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't specify your age!\nPlease enter your age!").setFooter("Progress: 6%", null).build() : new EmbedBuilder().setTitle("Age").setDescription("Please enter your age").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 6%", null).build()))
                        .onClick("\u2705", (done, age1) -> {
                            if (!hasClickedAge[0]) {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        ageHistory.add(messages.getContentRaw());
                                        main.getLogger().debug("Added ageHistory\n" + ageHistory);
                                    }
                                    if (!ageHistory.isEmpty()) {
                                        main.getLogger().debug(messageIds.toString());
                                        messageIds.forEach(id -> {
                                            success.getMessageById(id).queue(editMsg -> {
                                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Age**: " + ageHistory.get(0), false);
                                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                                                main.getLogger().debug("inside of askAge msgUpdate");
                                            });
                                        });
                                    } else {
                                        executorService.submit(() -> {
                                            usernameError[0] = true;
                                            this.run();
                                        });
                                    }
                                });
                            }
                            if (!ageError[0]) {
                                askTimezone(main, success, channel);
                                hasClickedAge[0] = true;
                            } else {
                                ageError[0] = true;
                                this.run();
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askTimezone(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> timezoneHistory = new ArrayList<>();
                ReactionMenu timezoneMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (timezoneError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't specify your timezone!\nPlease enter your timezone!").setFooter("Progress: 13%", null).build() : new EmbedBuilder().setTitle("Timezone").setDescription("Please enter your timezone").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 13%", null).build()))
                        .onClick("\u2705", (done, timezone1) -> {
                            if (!hasClickedTimezone[0]) {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        timezoneHistory.add(messages.getContentRaw());
                                    }
                                    if (!timezoneHistory.isEmpty()) {
                                        messageIds.forEach(id -> {
                                            success.getMessageById(id).queue(editMsg -> {
                                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Timezone**: " + timezoneHistory.get(0), false);
                                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                                            });
                                        });
                                    } else {
                                        executorService.submit(() -> {
                                            timezoneError[0] = true;
                                            this.run();
                                        });
                                    }
                                });
                            }
                            if (!timezoneError[0]) {
                                askLength(main, success, channel);
                                hasClickedTimezone[0] = true;
                            } else {
                                timezoneError[0] = true;
                                this.run();
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askLength(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> lengthHistory = new ArrayList<>();
                ReactionMenu lengthMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (lengthError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't specify how long you've played on the ShadowNode network!\nPlease enter your playtime!").setFooter("Progress: 20%", null).build() : new EmbedBuilder().setTitle("Playtime").setDescription("Please enter how long you've played on the ShadowNode network!").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 20%", null).build()))
                        .onClick("\u2705", (done, length1) -> {
                            if (!hasClickedLength[0]) {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        lengthHistory.add(messages.getContentRaw());
                                    }
                                    if (!lengthHistory.isEmpty()) {
                                        messageIds.forEach(id -> {
                                            success.getMessageById(id).queue(editMsg -> {
                                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**ShadowNode Playtime**: " + lengthHistory.get(0), false);
                                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                                            });
                                        });
                                    } else {
                                        executorService.submit(() -> {
                                            lengthError[0] = true;
                                            this.run();
                                        });
                                    }
                                });
                            }
                            if (!lengthError[0]) {
                                askModExp(main, success, channel);
                                hasClickedLength[0] = true;
                            } else {
                                lengthError[0] = true;
                                this.run();
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askModExp(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> modExpHistory = new ArrayList<>();
                ReactionMenu modExpMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (modExpError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't specify your experience in Moderation!\nPlease enter your moderation experience!").setFooter("Progress: 26%", null).build() : new EmbedBuilder().setTitle("Moderation Experience").setDescription("Please enter your experience in moderating Minecraft servers").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 26%", null).build()))
                        .onClick("\u2705", (done, modExp1) -> {
                            if (!hasClickedModExp[0]) {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        modExpHistory.add(messages.getContentRaw());
                                    }
                                    if (!modExpHistory.isEmpty()) {
                                        messageIds.forEach(id -> {
                                            success.getMessageById(id).queue(editMsg -> {
                                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "\n**Moderation Experience**: " + modExpHistory.get(0), false);
                                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                                            });
                                        });
                                    } else {
                                        executorService.submit(() -> {
                                            modExpError[0] = true;
                                            this.run();
                                        });
                                    }
                                });
                            }
                            if (!modExpError[0]) {
                                askActiveServer(main, success, channel);
                                hasClickedModExp[0] = true;
                            } else {
                                modExpError[0] = true;
                                this.run();
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askActiveServer(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> activeServerHistory = new ArrayList<>();
                ReactionMenu activeServerMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (activeServerError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't specify which server you're most active on!\nPlease enter the name of the server in which you are the most active!").setFooter("Progress: 33%", null).build() : new EmbedBuilder().setTitle("Activity").setDescription("Please enter the name of the server on which you are the most active").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 33%", null).build()))
                        .onClick("\u2705", (done, activeServer1) -> {
                            if (!hasClickedActiveServer[0]) {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        activeServerHistory.add(messages.getContentRaw());
                                    }
                                    if (!activeServerHistory.isEmpty()) {
                                        messageIds.forEach(id -> {
                                            success.getMessageById(id).queue(editMsg -> {
                                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Most Active Server**: " + activeServerHistory.get(0), false);
                                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                                            });
                                        });
                                    } else {
                                        executorService.submit(() -> {
                                            activeServerError[0] = true;
                                            this.run();
                                        });
                                    }
                                });
                            }
                            if (!activeServerError[0]) {
                                askCannotConnect(main, success, channel);
                                hasClickedActiveServer[0] = true;
                            } else {
                                activeServerError[0] = true;
                                this.run();
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askCannotConnect(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> cannotConnectHistory = new ArrayList<>();
                ReactionMenu cantConnectMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (cannotConnectError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't specify which you absolutely cannot connect to. If this answer is none, please enter `N/A`!\nPlease enter the name of the server in which you are the most active!").setFooter("Progress: 40%", null).build() : new EmbedBuilder().setTitle("Activity").setDescription("Please enter the name(s) of the server(s) you cannot connect to. Seperate the names by a comma.\n\nIf this is none, please enter `N/A`").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 40%", null).build()))
                        .onClick("\u2705", (done, cannotConnect1) -> {
                            if (!hasClickedCannotConnect[0]) {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        cannotConnectHistory.add(messages.getContentRaw());
                                    }
                                    if (!cannotConnectHistory.isEmpty()) {
                                        messageIds.forEach(id -> {
                                            success.getMessageById(id).queue(editMsg -> {
                                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Servers unable to Connect To**: " + cannotConnectHistory.get(0), false);
                                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                                            });
                                        });
                                    } else {
                                        executorService.submit(() -> {
                                            cannotConnectError[0] = true;
                                            this.run();
                                        });
                                    }
                                });
                            }
                            if (!cannotConnectError[0]) {
                                askMiniModding(main, success, channel);
                                hasClickedCannotConnect[0] = true;
                            } else {
                                cannotConnectError[0] = true;
                                this.run();
                            }
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askMiniModding(ShadowRewrite main, PrivateChannel success, TextChannel channel) { // C CORRECT
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> miniModdingHistory = new ArrayList<>();
                ReactionMenu miniModdingMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (miniModdingError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't answer. Enter **just the letter**!\nWhat is MiniModding?").addField("", "A. Adding client-side mods", false).addField("", "B. Helping other players with basic mod questions", false).addField("", "C. Telling a player details of the rules without consulting the staff", false).addField("", "D. All of the Above", false).setFooter("Progress: 46%", null).build() : new EmbedBuilder().setTitle("MiniModding").setDescription("What is MiniModding? Answer by entering **just the letter**").addField("", "A. Adding client-side mods", false).addField("", "B. Helping other players with basic mod questions", false).addField("", "C. Telling a player details of the rules without consulting the staff", false).addField("", "D. All of the Above", false).setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 46%", null).build()))
                        .onClick("\uD83C\uDDE6", (done, miniModding1) -> {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**MiniModding**: A", false);
                                });
                            askRules(main, success, channel);
                            hasClickedMiniModding[0] = true;
                        }).onClick("\uD83C\uDDE7", (done, miniModding1) -> {
                            success.getHistory().retrievePast(5).queue((history) -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**MiniModding**: B", false);
                                });
                            askRules(main, success, channel);
                            hasClickedMiniModding[0] = true;
                        }).onClick("\uD83C\uDDE8", (done, miniModding1) -> {
                            success.getHistory().retrievePast(5).queue((history) -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**MiniModding**: C", false);
                            });
                            askRules(main, success, channel);
                            hasClickedMiniModding[0] = true;
                        }).onClick("\uD83C\uDDE9", (done, miniModding1) -> {
                            success.getHistory().retrievePast(5).queue((history) -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**MiniModding**: D", false);
                            });
                            askRules(main, success, channel);
                            hasClickedMiniModding[0] = true;
                        }).buildAndDisplayForPrivateMessage(success);

            }
        });
    }

    private void askRules(ShadowRewrite main, PrivateChannel success, TextChannel channel) { // D Correct
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> ruleHistory = new ArrayList<>();
                ReactionMenu rulesMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (rulesError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't answer. Enter **just the letter**!\nWhere are all the places the basic rules can be found?").addField("", "A. In server spawns", false).addField("", "B. The Website", false).addField("", "C. Discord", false).addField("", "D. All of the above", false).setFooter("Progress: 53%", null).build() : new EmbedBuilder().setTitle("Rule Location").setDescription("Where are all the places the basic rules can be found?").addField("", "A. In server spawns", false).addField("", "B. The Website", false).addField("", "C. Discord", false).addField("", "D. All of the above", false).setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 53%", null).build()))
                        .onClick("\uD83C\uDDE6", (done, rules1) -> {
                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Rules Location**: A", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                            });
                            askMobSpawners(main, success, channel);
                            hasClickedRules[0] = true;
                        }).onClick("\uD83C\uDDE7", (done, rules1) -> {
                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Rules Location**: B", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                            });
                            askMobSpawners(main, success, channel);
                            hasClickedRules[0] = true;
                        }).onClick("\uD83C\uDDE8", (done, rules1) -> {
                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Rules Location**: C", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                            });
                            askMobSpawners(main, success, channel);
                            hasClickedRules[0] = true;
                        }).onClick("\uD83C\uDDE9", (done, rules1) -> {
                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Rules Location**: D", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                            });
                            askMobSpawners(main, success, channel);
                            hasClickedRules[0] = true;
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askMobSpawners(ShadowRewrite main, PrivateChannel success, TextChannel channel) { // False
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> mobSpawnerHistory = new ArrayList<>();
                ReactionMenu mobSpawnerMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (mobSpawnersError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't answer!\nTrue or False:\nMob spawners do not need to autokill/autoloot if mobs do not spawn rapidly.").setFooter("Progress: 60%", null).build() : new EmbedBuilder().setTitle("Mob Spawners").setDescription("True or False:\nMob spawners do not need to autokill/autoloot if mobs do not spawn rapidly.").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 60%", null).build()))
                        .onClick("\uD83C\uDDF9", (done, mobSpawner1) -> {
                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Mob Spawner Knowledge**: True", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                            });
                            askAutomining(main, success, channel);
                            hasClickedMobSpawners[0] = true;
                        }).onClick("\uD83C\uDDEB", (done, mobSpawner1) -> {
                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Mob Spawner Knowledge**: False", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                            });
                                askAutomining(main, success, channel);
                            hasClickedMobSpawners[0] = true;
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askAutomining(ShadowRewrite main, PrivateChannel success, TextChannel channel) { // True
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> autominingHistory = new ArrayList<>();
                ReactionMenu automminingMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (autoMiningError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't answer!\nTrue or False:\nAutomining systems are allowed in mining dimensions").setFooter("Progress: 66%", null).build() : new EmbedBuilder().setTitle("Auto Miners").setDescription("True or False:\nAutomining systems are allowed in mining dimensions").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 66%", null).build()))
                        .onClick("\uD83C\uDDF9", (done, autoMining1) -> {

                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Automining Knowledge**: True", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                            });
                            askGriefing(main, success, channel);
                            hasClickedAutoMining[0] = true;
                        }).onClick("\uD83C\uDDEB", (done, autoMining1) -> {
                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Automining Knowledge**:  False", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                            });
                            askGriefing(main, success, channel);
                            hasClickedAutoMining[0] = true;
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askGriefing(ShadowRewrite main, PrivateChannel success, TextChannel channel) { // False
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> griefingHistory = new ArrayList<>();
                ReactionMenu griefingMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (griefingError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't answer!\nTrue or False:\nGriefing is allowed if a base is **NOT** claimed.").setFooter("Progress: 80%", null).build() : new EmbedBuilder().setTitle("Griefing").setDescription("True or False:\nGriefing is allowed if a base is **NOT** claimed.").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 80%", null).build()))
                        .onClick("\uD83C\uDDF9", (done, autoMining1) -> {
                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Griefing Knowledge**: True", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                            });
                            askFriend(main, success, channel);
                            hasClickedGriefing[0] = true;
                        }).onClick("\uD83C\uDDEB", (done, autoMining1) -> {
                            success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                identificationEmbedBuilder = identificationEmbedBuilder.addField("", "**Griefing Knowledge**: False", false);
                                executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                                });
                                askFriend(main, success, channel);
                            hasClickedGriefing[0] = true;
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askFriend(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> friendHistory = new ArrayList<>();
                ReactionMenu friendMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (friendError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't answer!\nIf your friend was breaking our server rules and you (as a staff member) know about it, what do you do?").setFooter("Progress: 86%", null).build() : new EmbedBuilder().setTitle("Situational Question").setDescription("If your friend was breaking our server rules and you (as a staff member) know about it, what do you do?").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 86%", null).build()))
                        .onClick("\u2705", (done, autoMining1) -> {
                            if (!griefingError[0]) {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        friendHistory.add(messages.getContentRaw());
                                    }
                                    if (!friendHistory.isEmpty()) {
                                        identificationEmbedBuilder = identificationEmbedBuilder.appendDescription("**If your friend was breaking our server rules and you (as a staff member) know about it, what do you do?**\n " + friendHistory.get(0));
                                    }
                                });
                            } else {
                                executorService.submit(() -> {
                                    friendError[0] = true;
                                    this.run();
                                });
                            }
                            if (!friendError[0])
                                askPriorities(main, success, channel);
                            else {
                                friendError[0] = true;
                                this.run();
                            }
                            hasClickedFriend[0] = true;
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askPriorities(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> prioritiesHistory = new ArrayList<>();
                ReactionMenu prioritiesMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (prioritiesError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't answer!\nYou are on Stone Block assisting one player with a past grief, another player on the server has found a game-breaking bug and a player on DW20 is griefing players/killing them and it requires immediate attention, you are the only staff online right now, what is AN efficient course of action to take in your given situation?").setFooter("Progress: 86%", null).build() : new EmbedBuilder().setTitle("Situational Question").setDescription("You are on Stone Block assisting one player with a past grief, another player on the server has found a game-breaking bug and a player on DW20 is griefing players/killing them and it requires immediate attention, you are the only staff online right now, what is AN efficient course of action to take in your given situation?").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 86%", null).build()))
                        .onClick("\u2705", (done, autoMining1) -> {
                            if (!griefingError[0]) {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        prioritiesHistory.add(messages.getContentRaw());
                                    }
                                    if (!prioritiesHistory.isEmpty()) {
                                        success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                            identificationEmbedBuilder = identificationEmbedBuilder.appendDescription("\n\n**You are on Stone Block assisting one player with a past grief, another player on the server has found a game-breaking bug and a player on DW20 is griefing players/killing them and it requires immediate attention, you are the only staff online right now, what is AN efficient course of action to take in your given situation?**\n " + prioritiesHistory.get(0));
                                            executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.setFooter("Correct Answers: C, D, False, True, False", null).build()).queue());
                                        });
                                    } else {
                                        executorService.submit(() -> {
                                            prioritiesError[0] = true;
                                            this.run();
                                        });
                                    }
                                });
                            }
                            if (!prioritiesError[0])
                                askHostile(main, success, channel);
                            else {
                                prioritiesError[0] = true;
                                this.run();
                            }
                            hasClickedPriorities[0] = true;
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askHostile(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> hostileHistory = new ArrayList<>();
                ReactionMenu hostileMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (hostileError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't answer!\nYou are dealing with a player who has repeated their question to numerous members of staff via Dms and a number of different channels as well as in-game, the member becomes a little hostile in their frustration and efforts but DOES NOT break any rules which would warrant a ban/removal, what is a possible way you can step in and resolve this situation?").setFooter("Progress: 93%", null).build() : new EmbedBuilder().setTitle("Situational Question").setDescription("You are dealing with a player who has repeated their question to numerous members of staff via Dms and a number of different channels as well as in-game, the member becomes a little hostile in their frustration and efforts but DOES NOT break any rules which would warrant a ban/removal, what is a possible way you can step in and resolve this situation?").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 93%", null).build()))
                        .onClick("\u2705", (done, autoMining1) -> {
                            if (!hostileError[0]) {
                                success.getHistory().retrievePast(5).queue((history) -> {
                                    for (Message messages : history) {
                                        if (messages.getAuthor().isBot())
                                            break;
                                        hostileHistory.add(messages.getContentRaw());
                                    }
                                    if (!hostileHistory.isEmpty()) {
                                        success.getMessageById(messageIds.get(0)).queue(editMsg -> {
                                            identificationEmbedBuilder = identificationEmbedBuilder.appendDescription("\n\n**You are dealing with a player who has repeated their question to numerous members of staff via Dms and a number of different channels as well as in-game, the member becomes a little hostile in their frustration and efforts but DOES NOT break any rules which would warrant a ban/removal, what is a possible way you can step in and resolve this situation?**\n " + hostileHistory.get(0));
                                            executorService.submit(() -> editMsg.editMessage(identificationEmbedBuilder.build()).queue());
                                        });
                                    } else {
                                        executorService.submit(() -> {
                                            hostileError[0] = true;
                                            this.run();
                                        });
                                    }
                                });
                            }
                            if (!hostileError[0])
                                askFinished(main, success, channel);
                            else {
                                hostileError[0] = true;
                                this.run();
                            }
                            hasClickedHostile[0] = true;
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }

    private void askFinished(ShadowRewrite main, PrivateChannel success, TextChannel channel) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                main.getMessenger().sendEmbed(success, identificationEmbedBuilder.build());
                ReactionMenu finishedMenu = new ReactionMenu.PrivateMessageReactionMenu.Builder(main.getJDA()).setEmbed(
                        (finishedError[0] ? new EmbedBuilder().setTitle("Error!").setColor(Color.RED).setDescription("You didn't answer!\nAre you ready to submit your application? React to \u2705 in order to send your app").setFooter("Progress: 100%", null).build() : new EmbedBuilder().setTitle("Finished").setDescription("Are you ready to submit your application? React to \u2705 in order to send your app").setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString())).setFooter("Progress: 100%", null).build()))
                        .onClick("\u2705", (done, autoMining1) -> {

                            if (!finishedError[0]) {
                                main.getMessenger().sendEmbed(success, EmbedTemplates.PRETTY_SUCCESSFULL.getEmbed().setDescription("Submitted Staff Application\nA senior member of the staff team will message you with an update regarding your application in the coming days").build());

                                webhookMessage = webhookBulder.addEmbeds(identificationEmbedBuilder.build()).setUsername("ApplicationBot").setAvatarUrl("https://www.shadownode.ca/favicon.ico").build();
                                client.send(webhookMessage);
                                client.close();
                            }
                            else {
                                finishedError[0] = true;
                                this.run();
                            }
                            hasClickedFriend[0] = true;
                        }).buildAndDisplayForPrivateMessage(success);
            }
        });
    }
}
