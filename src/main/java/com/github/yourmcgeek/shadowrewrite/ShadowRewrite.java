package com.github.yourmcgeek.shadowrewrite;

import com.github.yourmcgeek.shadowrewrite.commands.admin.*;
import com.github.yourmcgeek.shadowrewrite.commands.application.StaffApplicationCommand;
import com.github.yourmcgeek.shadowrewrite.commands.support.*;
import com.github.yourmcgeek.shadowrewrite.commands.support.admin.TransferCommand;
import com.github.yourmcgeek.shadowrewrite.commands.wiki.*;
import com.github.yourmcgeek.shadowrewrite.listeners.discord.*;
import com.github.yourmcgeek.shadowrewrite.listeners.redis.RedisClient;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import me.bhop.bjdautilities.Messenger;
import me.bhop.bjdautilities.command.CommandHandler;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.hooks.InterfacedEventManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;

public class ShadowRewrite {

    private Path logDirectory;
    private Path supportLogDirectory;
    private Path attachmentDir;
    private Path appDir;
    private Messenger messenger;
    private Path directory;
    private Path configDirectory;
    private final ShadowRewrite bot = this;
    private Config mainConfig;
    private JsonObject mainConf;
    private Logger logger;
    private JDA jda;
    private RedisClient redisClient;
    private boolean ticketsEnabled = true;
    private final CommandResponses responses = new CommandResponses();
    private final ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(2);

    public void init(Path directory, Path configDirectory) throws Exception {
        this.directory = directory;
        this.configDirectory = configDirectory;
        logger = LoggerFactory.getLogger("ShadowBot");
        logger.info("Initializing Config!");

        try {
            initConfig(configDirectory);
        } catch (Exception e) {
            throw new RuntimeException("Failed to initialize mainConfig!", e);
        }

        try {

            if (mainConfig.getConfigValue("token").getAsString().equals("add_me")) {
                System.out.println("Please add the bot token into the Config.");
                System.exit(1);
            }

            logger.info("Setting Preferences...");
            this.jda = new JDABuilder(AccountType.BOT)
                    .setToken(mainConfig.getConfigValue("token").getAsString())
                    .setEventManager(new ThreadedEventManager())
                    .setGame(Game.playing("play.shadownode.ca"))
                    .build();
            jda.awaitReady();

            logger.info("Starting Messenger...");
            this.messenger = new Messenger();

            logger.info("Registering Commands...");

            CommandHandler handler = new CommandHandler.Builder(jda).setGenerateHelp(true).addCustomParameter(this).setResponses(responses).setEntriesPerHelpPage(6).guildIndependent().setCommandLifespan(10).setPrefix(getPrefix()).setResponseLifespan(90).build();

            handler.register(new SupportSetup());
            handler.register(new ReloadConfigCommand());
            handler.register(new LogChannelCommand());
            handler.register(new LinkAccount());
            handler.register(new CrashReport());
            handler.register(new Restart());
            handler.register(new Claiming());
            handler.register(new ChunkLoading());
            handler.register(new Wiki());
            handler.register(new Relocate());
            handler.register(new Crate());
            handler.register(new EmbedCommand());
            handler.register(new OnlineEmbedCommand());
            handler.register(new TicketToggleCommand());
            handler.register(new UsernameCommand());
            handler.register(new ServerCommand());
            handler.register(new StaffApplicationCommand());
            handler.register(new TransferCommand());
            handler.register(new RolesEmbedCommand());
            handler.register(new EmoteCommand());
            handler.register(new LogCommand());
            handler.register(new AddRoleCommand());
            logger.info("Registering Listeners...");
            this.jda.addEventListener(new TicketCreationListener(this));
            this.jda.addEventListener(new SupportCategoryListener(this));
            this.jda.addEventListener(new TicketChannelsReactionListener(this));
            this.jda.addEventListener(new SuggestionListener(this));
            this.jda.addEventListener(new RoleReactionListener(this));
            this.jda.addEventListener(new JoinGuildListener(this));

        } catch (LoginException e) {
            this.getLogger().error("", e);
        }

        scheduledExecutorService.scheduleAtFixedRate(() -> {
            Objects.requireNonNull(jda.getGuildById(getMainConfig().getConfigValue("guildID").getAsLong()).getDefaultChannel())
                    .createInvite()
                    .setMaxAge(getMainConfig().getConfigValue("inviteRefreshTimer").getAsLong(), TimeUnit.MINUTES)
                    .complete();
        }, 0, getMainConfig().getConfigValue("inviteRefreshTimer").getAsInt(), TimeUnit.MINUTES);

        try {
            logger.info("Checking Log directories...");
            Path logs = Paths.get(directory + "/logs");
            if (!Files.exists(logs)) {
                Files.createDirectories(logs);
                logDirectory = logs;

            }
            logDirectory = logs;
            Path support = logs.resolve("supportTickets");
            if (!Files.exists(support)) {
                Files.createDirectories(support);
                supportLogDirectory = support;
            }
            supportLogDirectory = support;
            Path attachments = support.resolve("attachments");
            if (!Files.exists(attachments)) {
                Files.createDirectories(attachments);
                attachmentDir = attachments;
            }
            attachmentDir = attachments;

            logger.info("Checking application directory...");
            Path app = Paths.get(directory + "/apps");
            if (!Files.exists(app)) {
                Files.createDirectories(app);
                appDir = app;
            }
            appDir = app;
        } catch (IOException e) {
            logger.error("Error creating directories!", e);
        }

        logger.info("Everything Loaded Successfully | Ready to accept input!");
    }

    public List<String[]> getTips() {
        JsonArray tips = mainConfig.getConfigValue("tips");
        List<String[]> tipArray = new ArrayList<>();
        for (Object obj : tips) {
            JsonObject jsonObject = (JsonObject) obj;
            String word = jsonObject.get("word").getAsString();
            String suggestion = jsonObject.get("suggestion").getAsString();
            String[] put = new String[]{word, suggestion};
            tipArray.add(put);
        }
        return tipArray;
    }

    public List<String[]> getCustomChat() {
        JsonArray customChat = mainConfig.getConfigValue("customChatCommands");
        List<String[]> customChatArray = new ArrayList<>();
        for (Object obj : customChat) {
            JsonObject jsonObject = (JsonObject) obj;
            String command = jsonObject.get("command").getAsString();
            String result = jsonObject.get("result").getAsString();
            String[] put = new String[]{command, result};
            customChatArray.add(put);
        }
        return customChatArray;
    }

    public void shutdown() {
        logger.info("Initiating Shutdown...");
        getJDA().shutdown();
        logger.info("Shutdown Complete.");
    }

    public void reloadConfig() {
        try {
            this.initConfig(getDirectory());
        } catch (Exception e) {
            getLogger().error("", e);
        }
    }

    private void initConfig(Path configDirectory) {
        try {
            mainConfig = new Config(this, configDirectory);
            mainConf = mainConfig.newConfig("config", Config.writeDefaults());
        } catch (Exception e) {
            this.getLogger().error("", e);
        }
    }

    public Path getDirectory() {
        return directory;
    }

    public JDA getJDA() {
        return this.jda;
    }

    public JsonObject getMainConf() {
        return mainConf;
    }

    public Config getMainConfig() {
        return mainConfig;
    }

    public String getPrefix() {
        return mainConfig.getConfigValue("commandPrefix").getAsString();
    }

    public Logger getLogger() {
        return logger;
    }

    public boolean isTicketsEnabled() {
        return ticketsEnabled;
    }

    public void setTicketsEnabled(boolean ticketsEnabled) {
        this.ticketsEnabled = ticketsEnabled;
    }

    public RedisClient getClient() {
        return redisClient;
    }

    public Path getLogDirectory() {
        return logDirectory;
    }

    public Messenger getMessenger() {
        return messenger;
    }

    public long getGuildID() {
        return mainConfig.getConfigValue("guildID").getAsLong();
    }

    public Path getAttachmentDir() {
        return attachmentDir;
    }

    public Path getSupportLogDirectory() {
        return supportLogDirectory;
    }

    private final class ThreadedEventManager extends InterfacedEventManager {

        private final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);

        @Override
        public void handle(Event e) {
            executor.submit(() -> super.handle(e));
        }
    }
}
