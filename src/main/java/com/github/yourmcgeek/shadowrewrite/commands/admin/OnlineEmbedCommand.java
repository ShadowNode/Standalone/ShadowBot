package com.github.yourmcgeek.shadowrewrite.commands.admin;

import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;

import java.awt.*;
import java.util.List;

@Command(label = {"onlineembed", "onlinemessage"}, usage = "onlineembed {jsoncode}", minArgs = 1, description = "Sends an embed based on the json!", hideInHelp = true)
public class OnlineEmbedCommand {

    @Execute
    public CommandResult onExecute(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowRewrite main) {
        if (member.getRoles().stream().map(Role::getName).anyMatch(s -> s.equalsIgnoreCase("Staff") || s.equalsIgnoreCase("Developer") || s.equalsIgnoreCase("Owner"))) {
            if (args.size() > 0) {
                JsonObject embedMessage = new JsonParser().parse(String.join(" ", args)).getAsJsonObject();
                EmbedBuilder embedBuilder = new EmbedBuilder();
                if (embedMessage.has("title")) {
                    embedBuilder.setTitle(embedMessage.get("title").getAsString(), (embedMessage.has("url") ? (embedMessage.get("url").getAsString().contains("http://") || embedMessage.get("url").getAsString().contains("https://") ? embedMessage.get("url").getAsString() : "http://" + embedMessage.get("url").getAsString()) : null));
                }
                if (embedMessage.has("description")) {
                    embedBuilder.setDescription(embedMessage.get("description").getAsString());
                }
                if (embedMessage.has("author")) {
                    JsonObject author = embedMessage.getAsJsonObject("author");
                    embedBuilder.setAuthor(author.get("name").getAsString(), (author.has("url") ? (embedMessage.get("url").getAsString().contains("http://") || author.get("url").getAsString().contains("https://") ? author.get("url").getAsString() : "http://" + author.get("url").getAsString()) : null), (author.has("icon_url") ? (author.get("icon_url").getAsString().contains("http://") || author.get("icon_url").getAsString().contains("https://") ? author.get("icon_url").getAsString() : "http://" + author.get("icon_url").getAsString()) : null));
                }
                if (embedMessage.has("color")) {
                    embedBuilder.setColor(embedMessage.get("color").getAsInt());
                }
                if (embedMessage.has("footer")) {
                    JsonObject footer = embedMessage.getAsJsonObject("footer");
                    embedBuilder.setFooter(footer.get("text").getAsString(), (footer.has("icon_url") ? (footer.get("icon_url").getAsString().contains("http://") || footer.get("icon_url").getAsString().contains("https://") ? footer.get("icon_url").getAsString() : "http://" + footer.get("icon_url").getAsString()) : null));
                }
                if (embedMessage.has("thumbnail")) {
                    embedBuilder.setThumbnail((embedMessage.get("thumbnail").getAsString().contains("http://") || embedMessage.get("thumbnail").getAsString().contains("https://") ? embedMessage.get("thumbnail").getAsString() : "http://" + embedMessage.get("thumbnail").getAsString()));
                }
                if (embedMessage.has("image")) {
                    embedBuilder.setImage((embedMessage.get("image").getAsString().contains("http://") || embedMessage.get("image").getAsString().contains("https://") ? embedMessage.get("image").getAsString() : "http://" + embedMessage.get("image").getAsString()));
                }
                if (embedMessage.has("fields")) {
                    JsonArray fields = embedMessage.getAsJsonArray("fields");
                    for (Object object : fields) {
                        JsonObject field = (JsonObject) object;
                        embedBuilder.addField(field.get("name").getAsString(), field.get("value").getAsString(), field.get("inline").getAsBoolean());
                    }
                }
                channel.sendMessage(embedBuilder.build()).queue();
            } else {
                main.getMessenger().sendEmbed(channel, new EmbedBuilder().setColor(Color.RED).setTitle("Error").setDescription("This command use the Nadekobot online embed generator. To make an embed go to [Nadeko](http://embedbuilder.nadekobot.me) and build the embed how you would like and then copy the json and paste it after this command!").build(), 30);
            }
            return CommandResult.success();
        } else {
            return CommandResult.noPermission();
        }
    }
}
