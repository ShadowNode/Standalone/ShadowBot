package com.github.yourmcgeek.shadowrewrite.commands.support.admin;

import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import me.bhop.bjdautilities.EditableMessage;
import me.bhop.bjdautilities.ReactionMenu;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Command(label = "transfer", usage = "transfer @User#Discriminator", description = "Transfers an open support ticket to the mentioned user", minArgs = 1, hideInHelp = true)
public class TransferCommand {

    @Execute
    public CommandResult onExecute(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowRewrite main) {
        if (!member.getRoles().stream().map(Role::getName).anyMatch(s -> s.equalsIgnoreCase("Staff") || s.equalsIgnoreCase("Developer") || s.equalsIgnoreCase("Owner"))) {
            return CommandResult.noPermission();
        } else {
            if (args.isEmpty())
                return CommandResult.invalidArguments();
            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM/dd/YY");
            String[] split = channel.getTopic().split(" ");
            long messageId = Long.parseLong(split[8]);
            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(args.get(0));
            String mentionId = "";
            while (m.find()) {
                mentionId = m.group();
            }

            String finalMentionId = mentionId;
            channel.getMessageById(messageId).queue(msg -> {
                ReactionMenu reaction = new ReactionMenu.Import(msg).build();
                EditableMessage originalMessage = reaction.getMessage();
                EmbedBuilder embedBuilder = new EmbedBuilder()
                        .setFooter(originalMessage.getEmbeds().get(0).getFooter().getText(), originalMessage.getEmbeds().get(0).getFooter().getProxyIconUrl())
                        .setColor(originalMessage.getEmbeds().get(0).getColorRaw())
                        .addField(originalMessage.getEmbeds().get(0).getFields().get(0).getName(), channel.getJDA().getUserById(finalMentionId).getAsMention(), true) // Author
                        .addField(originalMessage.getEmbeds().get(0).getFields().get(1).getName(), originalMessage.getEmbeds().get(0).getFields().get(1).getValue(), true) // Username
                        .addField(originalMessage.getEmbeds().get(0).getFields().get(2).getName(), originalMessage.getEmbeds().get(0).getFields().get(2).getValue(), true) // UUID
                        .addField(originalMessage.getEmbeds().get(0).getFields().get(3).getName(), originalMessage.getEmbeds().get(0).getFields().get(3).getValue(), true) // Server
                        .addField(originalMessage.getEmbeds().get(0).getFields().get(4).getName(), originalMessage.getEmbeds().get(0).getFields().get(4).getValue(), true); // Ticket
                reaction.getMessage().setContent(embedBuilder.build());
                channel.getManager().setTopic("Creation date " + channel.getCreationTime().format(dateFormat) + " Authors ID: " + finalMentionId + " Message ID: " + reaction.getMessage().getIdLong() + " Channel ID: " + channel.getIdLong()).queue();
            });
            return CommandResult.success();
        }
    }
}
