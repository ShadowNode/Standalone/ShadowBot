package com.github.yourmcgeek.shadowrewrite.commands.admin;

import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.lang.reflect.Member;
import java.util.List;

@Command(label = {"reload", "configreload"}, permission = Permission.ADMINISTRATOR, hideInHelp = true)
public class ReloadConfigCommand {

    @Execute
    public CommandResult onConfigReload(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowRewrite bot) {
        if (args.size() > 0) return CommandResult.invalidArguments();
        try {
            bot.reloadConfig();
            bot.getMessenger().sendEmbed(channel, new EmbedBuilder().setDescription("Reloaded Config!").setColor(Color.GREEN).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return CommandResult.success();
    }

}
