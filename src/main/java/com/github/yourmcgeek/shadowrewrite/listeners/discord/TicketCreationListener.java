package com.github.yourmcgeek.shadowrewrite.listeners.discord;

import com.github.yourmcgeek.shadowrewrite.EmbedTemplates;
import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import me.bhop.bjdautilities.ReactionMenu;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class TicketCreationListener extends ListenerAdapter {

    private ShadowRewrite main;
    private int userCount;

    public TicketCreationListener(ShadowRewrite main) {
        this.main = main;
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM/dd/YY");
        if (event.getAuthor().isBot() || event.getChannel().getIdLong() != main.getMainConfig().getConfigValue("TicketCreationChannelID").getAsLong() || event.getMessage().getContentRaw().contains("supportsetup") || event.getMessage().getContentRaw().contains("setupsupport"))
            return;
        if (main.isTicketsEnabled()) {
            Member member = event.getJDA().getGuildById(main.getGuildID()).getMember(event.getAuthor());

            for (Guild.Ban bans : event.getJDA().getGuildById(main.getGuildID()).getBanList().complete()) {
                if (bans.getUser().getIdLong() == member.getUser().getIdLong())
                    return;
            }

            for (TextChannel channel : main.getJDA().getCategoryById(Long.parseLong(main.getMainConfig().getConfigValue("supportCategoryId").getAsString())).getTextChannels()) {
                if (channel.getName().startsWith(event.getAuthor().getName().toLowerCase())) {
                    userCount++;
                    if (userCount >= 2) {
                        event.getMessage().delete().queue();
                        main.getMessenger().sendEmbed(event.getChannel(), EmbedTemplates.ERROR.getEmbed().appendDescription("No channel has been created because you have multiple channels open already. Please complete these issue first!").build(), 10);
                        return;
                    }
                }
            }

            String userMessage = event.getMessage().getContentRaw();
            event.getMessage().delete().queue();

            TextChannel supportChannel = (TextChannel) event.getJDA().getCategoryById(main.getMainConfig().getConfigValue("supportCategoryId").getAsLong())
                    .createTextChannel(member.getEffectiveName() + "-" + ThreadLocalRandom.current().nextInt(99999)).complete();

            supportChannel.putPermissionOverride(member).setAllow(101440).complete();
            EmbedBuilder message = new EmbedBuilder()
                    .addField("Author: ", member.getAsMention(), true)
                    .addField("Username: ", "Run `" + main.getPrefix() + "username <Username>` to set this field", true)
                    .addField("UUID: ", "Run `" + main.getPrefix() + "username <Username>` to set this field", true)
                    .addField("Server:", "Run `" + main.getPrefix() + "server <Server>` to set this field", true)
                    .setFooter("If the ticket is resolved, please click \u2705. All staff and developers can close the ticket also.", event.getJDA().getSelfUser().getEffectiveAvatarUrl())
                    .setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString()));
            ReactionMenu supportMessage;
            if (userMessage.length() > 1024) {
                supportMessage = new ReactionMenu.Builder(event.getJDA()).setStartingReactions("\u2705", "\uD83D\uDD12").setEmbed(message.build()).setMessage(userMessage).buildAndDisplay(supportChannel);
            } else {
                supportMessage = new ReactionMenu.Builder(event.getJDA()).setStartingReactions("\u2705", "\uD83D\uDD12").setEmbed(message.addField("Ticket: ", userMessage, true).build()).buildAndDisplay(supportChannel);
            }
            supportChannel.getManager().setTopic("Creation date: " + supportChannel.getCreationTime().format(dateFormat) + " Authors ID: " + event.getAuthor().getIdLong() + " Message ID: " + supportMessage.getMessage().getIdLong() + " Channel ID: " + supportChannel.getIdLong()).queue();

            Runnable task = () -> main.getMessenger().sendEmbed(supportChannel, EmbedTemplates.ERROR.getEmbed().setDescription("**Remember to finish your ticket off by running the username and server commands!** ").build(), 15);
            executorService.schedule(task, 10, TimeUnit.SECONDS);


            for (Message.Attachment attachment : event.getMessage().getAttachments()) {
                try {
                    if (!new File(main.getSupportLogDirectory().toFile(), "attachments").exists()) {
                        new File(main.getSupportLogDirectory().toFile(), "attachments").mkdir();
                    }
                    attachment.download(new File(main.getSupportLogDirectory().toFile() + "/attachments/", attachment.getFileName()));
                    supportChannel.sendFile(new File(main.getAttachmentDir().toFile(), attachment.getFileName())).complete();
                } catch (Exception e) {
                    main.getLogger().error("", e);
                }
            }

            EmbedBuilder embedBuilder = new EmbedBuilder()
                    .setTitle("Support Channel")
                    .setDescription("If you would prefer this ticket to be private, or concerns a dupe, glitch, bug, or contribution payment please react in your ticket channel with \uD83D\uDD12 to only allow staff to view the ticket. ")
                    .addField("Ticket", "https://discordapp.com/channels/" + main.getGuildID() + "/" + supportChannel.getIdLong(), false)
                    .setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString()));
            main.getMessenger().sendEmbed(event.getChannel(), embedBuilder.build(), 10);
        } else {
            main.getMessenger().sendEmbed(event.getChannel(), EmbedTemplates.ERROR.getEmbed().setDescription("Tickets are currently disabled from being made!").build(), 10);
        }
    }
}