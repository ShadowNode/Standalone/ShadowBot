package com.github.yourmcgeek.shadowrewrite.commands.admin;

import com.github.yourmcgeek.shadowrewrite.EmbedTemplates;
import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import com.github.yourmcgeek.shadowrewrite.utils.HastebinUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.exceptions.UnirestException;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Command(label = "roles", permission = Permission.MESSAGE_WRITE, hideInHelp = true, description = "Run in roles channel where no user can send messages to setup the role embed for users to react upon", usage = "roles", minArgs = 1)
public class RolesEmbedCommand {

    @Execute
    public CommandResult onExecute(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowRewrite main) throws UnirestException {

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonObject config = main.getMainConf();
        JsonArray array = config.get("ReactionRoles").getAsJsonArray();
        switch (args.get(0).toLowerCase()) {
            case "add":
                main.getLogger().info(args.toString());
                // roles add @role \:emoteID:
                Pattern p = Pattern.compile("\\d+");
                Matcher m = p.matcher(args.get(2));
                String reactionID = "";
                while (m.find()) {
                    reactionID = m.group();
                }
                if (!args.get(2).startsWith("\\")) {
                    main.getMessenger().sendEmbed(channel, EmbedTemplates.ERROR.getEmbed().setDescription("Please put a `\\` in front of the emoji!").build());
                    return CommandResult.success();
                }
                JsonObject roleObject = new JsonObject();
                roleObject.addProperty("role", args.get(1));
                roleObject.addProperty("reactionID", reactionID);
                roleObject.addProperty("reaction", channel.getGuild().getEmoteById(reactionID).getName() + ":" + reactionID);
                array.add(roleObject);
                main.getMainConfig().saveConfig("config", config);
                main.reloadConfig();
                main.getMessenger().sendEmbed(channel, EmbedTemplates.SUCCESS.getEmbed().setDescription("Success! Added new Role Reaction, make sure to update #" + main.getMainConfig().getConfigValue("roleChannelName").getAsString()).build());
                return CommandResult.success();

            case "delete":
                // roles delete @role
                int a = 1;
                for (Object obj : array) {
                    JsonObject jsonObject = (JsonObject) obj;
                    if (args.get(1).equalsIgnoreCase(jsonObject.get("role").getAsString())) {
                        array.remove(jsonObject);
                        main.getMainConfig().saveConfig("config", config);
                        main.reloadConfig();
                        main.getMessenger().sendEmbed(channel, EmbedTemplates.SUCCESS.getEmbed().setDescription("Success! Removed Role Reaction, make sure to update " + main.getMainConfig().getConfigValue("roleChannelName").getAsString()).build());
                        return CommandResult.success();
                    } else {
                        a = 0;
                    }
                }
                if (a == 0) {
                    main.getMessenger().sendEmbed(channel, EmbedTemplates.ERROR.getEmbed().setDescription("Couldn't find " + args.get(1) + " in any active role reactions. Check spelling and try again").build());
                }

            case "list":
                main.getMessenger().sendEmbed(channel, EmbedTemplates.SUCCESS.getEmbed().setDescription(HastebinUtil.post(gson.toJson(array))).build());
                return CommandResult.success();
            case "update":
                try {
                    channel.getHistory().retrievePast(100).queue(msg -> channel.deleteMessages(msg).queue());
                    if (!channel.getName().equalsIgnoreCase(main.getMainConfig().getConfigValue("roleChannelName").getAsString())) {
                        main.getMessenger().sendEmbed(channel, EmbedTemplates.ERROR.getEmbed().setDescription("You need to run this inside of #" + main.getMainConfig().getConfigValue("roleChannelName").getAsString() + "!").build());
                        return CommandResult.success();
                    } else {
                        EmbedBuilder builder = new EmbedBuilder()
                                .setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString()))
                                .setTitle("React to gain the Role");
                        JsonArray ar = main.getMainConfig().getConfigValue("ReactionRoles").getAsJsonArray();
                        for (Object object2 : ar) {
                            JsonObject object1 = (JsonObject) object2;
//                builder.appendDescription(object1.get("reaction").getAsString() + " :: " + object1.get("role").getAsString());
                            builder.appendDescription(channel.getGuild().getEmoteById(object1.get("reactionID").getAsString()).getAsMention() + " :: " + object1.get("role").getAsString() + "\n");
                        }
                        channel.sendMessage(builder.build()).queue(msg -> {
                            for (Object object2 : array) {
                                JsonObject object1 = (JsonObject) object2;
                                msg.addReaction(object1.get("reaction").getAsString()).queue();
                            }
                        });
                        channel.sendMessage(EmbedTemplates.ERROR.getEmbed().setDescription("➼ You need to have `Show emoji reactions on messages` enabled in your settings\n➼ Please refresh Discord using **Ctrl+R** if the reactions are not visible\n➼ Spamming the reactions will result in punishment").build()).queue();
                        return CommandResult.success();
                    }
                } catch (Exception e) {
                    main.getLogger().error("", e);
                }
        }
        return CommandResult.success();
    }
}
