package com.github.yourmcgeek.shadowrewrite.listeners.discord;

import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class SupportCategoryListener extends ListenerAdapter {

    private ShadowRewrite main;

    public SupportCategoryListener(ShadowRewrite main) {
        this.main = main;
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("[dd/MM/YY HH:mm]");
        if (event.getChannel().getParent().getIdLong() == main.getMainConfig().getConfigValue("supportCategoryId").getAsLong()) {
            if (event.getChannel().getIdLong() != main.getMainConfig().getConfigValue("logChannelId").getAsLong()) {
                try {
                    if (!Files.exists(main.getSupportLogDirectory().resolve(event.getChannel().getName() + ".log"))) {
                        Files.createFile(main.getSupportLogDirectory().resolve(event.getChannel().getName() + ".log"));
                    }
                    StringBuilder content = new StringBuilder();
                    content.append("[").append(OffsetDateTime.now().format(format)).append("]");
                    if (!event.getMessage().getEmbeds().isEmpty()) {
                        for (MessageEmbed embed : event.getMessage().getEmbeds()) {
                            content.append(event.getMember().getEffectiveName()).append(": ").append("Embed").append(embed.toJSONObject());
                        }
                    }
                    if (!event.getMessage().getMentionedMembers().isEmpty()) {
                        String message = event.getMessage().getContentRaw();
                        for (Member mention : event.getMessage().getMentionedMembers())
                            message = message.replace(mention.getAsMention(), mention.getEffectiveName());
                        content.append(event.getMember().getEffectiveName()).append(": ").append(message);
                    } else {
                        content.append(event.getMember().getEffectiveName()).append(": ").append(event.getMessage().getContentRaw());
                    }
                    if (!event.getMessage().getAttachments().isEmpty()) {
                        for (Message.Attachment attachment : event.getMessage().getAttachments()) {
                            content.append(event.getMember().getEffectiveName()).append(": ").append("File: ").append(attachment.getProxyUrl());
                        }
                    }

                    Files.write(main.getSupportLogDirectory().resolve(event.getChannel().getName() + ".log"), (content.toString() + "\n").getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
                } catch (IOException e) {
                    main.getLogger().error("", e);
                }
            }
        }
    }
}