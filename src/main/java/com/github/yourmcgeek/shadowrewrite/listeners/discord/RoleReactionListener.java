package com.github.yourmcgeek.shadowrewrite.listeners.discord;

import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.MessageReaction;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class RoleReactionListener extends ListenerAdapter {

    private ShadowRewrite main;

    public RoleReactionListener(ShadowRewrite main) {
        this.main = main;
    }

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        if (event.isFromType(ChannelType.TEXT) && !event.getUser().isBot()) {
            TextChannel channel = (TextChannel) event.getChannel();
            if (main.getMainConfig().getConfigValue("debug").getAsBoolean())
                main.getLogger().debug("Inside event.isFromType(ChannelType.TEXT)");
            MessageReaction.ReactionEmote emote = event.getReactionEmote();
            if (channel.getName().equalsIgnoreCase(main.getMainConfig().getConfigValue("roleChannelName").getAsString())) {
                if (main.getMainConfig().getConfigValue("debug").getAsBoolean())
                    main.getLogger().debug("Inside channel name if statement");
                JsonArray array = main.getMainConfig().getConfigValue("ReactionRoles").getAsJsonArray();
                for (Object object : array) {
                    JsonObject object1 = (JsonObject) object;
                    if ((emote.getName() + ":" + emote.getId()).equalsIgnoreCase(object1.get("reaction").getAsString())) {
                        if (main.getMainConfig().getConfigValue("debug").getAsBoolean())
                            main.getLogger().debug("Inside for loop and if statement, adding role");
//                        event.getGuild().getController().addRolesToMember(event.getMember(), event.getJDA().getRolesByName(object1.get("role").getAsString(), true)).queue();
                        event.getGuild().getController().addSingleRoleToMember(event.getMember(), event.getJDA().getRolesByName(object1.get("role").getAsString(), true).get(0)).queue();
                        if (main.getMainConfig().getConfigValue("debug").getAsBoolean())
                            main.getLogger().debug("Added role");
                    }
                }
            }
        }
    }

    @Override
    public void onMessageReactionRemove(MessageReactionRemoveEvent event) {
        if (event.isFromType(ChannelType.TEXT)) {
            TextChannel channel = (TextChannel) event.getChannel();
            MessageReaction.ReactionEmote emote = event.getReactionEmote();
            if (channel.getName().equalsIgnoreCase(main.getMainConfig().getConfigValue("roleChannelName").getAsString())) {
                JsonArray array = main.getMainConfig().getConfigValue("ReactionRoles").getAsJsonArray();
                for (Object object : array) {
                    JsonObject object1 = (JsonObject) object;
                    if ((emote.getName() + ":" + emote.getId()).equalsIgnoreCase(object1.get("reaction").getAsString())) {
                        event.getGuild().getController().removeRolesFromMember(event.getMember(), event.getJDA().getRolesByName(object1.get("role").getAsString(), true)).queue();
                    }
                }
            }
        }
    }
}