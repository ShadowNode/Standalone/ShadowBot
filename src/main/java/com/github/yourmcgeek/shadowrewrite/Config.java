package com.github.yourmcgeek.shadowrewrite;

import com.google.gson.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class Config {

    private ShadowRewrite main;
    private Path configDirectory;
    private JsonElement conf;
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public Config(ShadowRewrite main, Path configDirectory) {
        this.main = main;
        this.configDirectory = configDirectory;
    }

    JsonObject newConfig(String configName, JsonObject configOptions) {
        JsonObject conf = null;
        Path config = configDirectory.resolve(configName + ".json");
        try {
            if (!Files.exists(config)) {
                Files.createFile(config);
                write(config, configOptions);
            }
            if (hasAllEntries(read(config).getAsJsonObject(), configOptions)) {
                conf = read(config).getAsJsonObject();
            } else {
                write(config, writeNotFoundDefaults(read(config).getAsJsonObject(), configOptions));
                conf = read(config).getAsJsonObject();
            }
        } catch (Exception e) {
            main.getLogger().error("", e);
        }
        this.conf = conf;
        return conf;
    }

    private JsonElement read(Path config) {
        JsonElement element = null;
        try (BufferedReader reader = Files.newBufferedReader(config)) {
            JsonParser parser = new JsonParser();
            element = parser.parse(reader);
        } catch (IOException e) {
            main.getLogger().error("", e);
        }
        return element;
    }

    private void write(Path config, JsonObject object) {
        try (BufferedWriter writer = Files.newBufferedWriter(config)) {
            writer.write(gson.toJson(object));
            writer.flush();
        } catch (IOException e) {
            main.getLogger().error("", e);
        }
    }

    public void saveConfig(String configName, JsonElement conf) {
        try (BufferedWriter writer = Files.newBufferedWriter(configDirectory.resolve(configName + ".json"))) {
            writer.write(gson.toJson(conf));
        } catch (Exception e) {
            main.getLogger().error("", e);
        }
    }

    public static JsonObject writeDefaults() {
        JsonObject jo = new JsonObject();
        jo.addProperty("token", "add_me");
        jo.addProperty("commandPrefix", "/");
        jo.addProperty("colour", "#ee82ee");
        jo.addProperty("supportCategoryId", "add_me");
        jo.addProperty("guildID", "add_me");
        jo.addProperty("logChannelId", "add_me");
        jo.addProperty("TicketCreationChannelID", "add_me");
        jo.addProperty("roleChannelName", "add_me");
        jo.addProperty("inviteRefreshTimer", 10);
        jo.addProperty("debug", false);
        jo.addProperty("StaffWebhook", "add_me");
        jo.addProperty("giveawayRoleID", "people with this role can start giveaways if they don't have ADMINISTRATOR perms via Discord");
        JsonObject timedRole = new JsonObject();
        timedRole.addProperty("roleToAdd", 0L);
        timedRole.addProperty("timeToWait", 10);
        jo.add("timedRole", timedRole);
        JsonArray roles = new JsonArray();
        JsonObject roleObject = new JsonObject();
        roleObject.addProperty("role", "roleName");
        roleObject.addProperty("reactionID", "\\:emoteName:");
        roleObject.addProperty("reaction", "emoteName");
        roles.add(roleObject);
        jo.add("ReactionRoles", roles);
        JsonObject redisSettings = new JsonObject();
        redisSettings.addProperty("hostname", "localhost");
        redisSettings.addProperty("port", 6379);
        redisSettings.addProperty("database", 0);
        redisSettings.addProperty("password", "password");
        redisSettings.addProperty("poolname", "master");
        redisSettings.add("sentinels", new JsonArray());
        jo.add("redis", redisSettings);
        JsonObject wiki = new JsonObject();
        wiki.addProperty("linkingURL", "https://www.shadownode.ca/threads/85/");
        wiki.addProperty("crashURL", "https://www.shadownode.ca/threads/102/");
        wiki.addProperty("claimURL", "https://shadownode.ca/threads/107/");
        wiki.addProperty("chunkURL", "https://shadownode.ca/threads/571/");
        wiki.addProperty("restartURL", "https://shadownode.ca/threads/153/");
        wiki.addProperty("wikiURL", "https://shadownode.ca/wiki/");
        wiki.addProperty("crateURL", "https://shadownode.ca/threads/89/");
        wiki.addProperty("relocateURL", "https://shadownode.ca/threads/724/");
        jo.add("wiki", wiki);
        JsonArray tips = new JsonArray();
        JsonObject tip1 = new JsonObject();
        tip1.addProperty("suggestion", "Visit our forums at <forum>");
        tip1.addProperty("word", "suggestion");
        JsonObject tip2 = new JsonObject();
        tip2.addProperty("suggestion", "My prefix is <prefix>");
        tip2.addProperty("word", "prefix");
        JsonObject tip3 = new JsonObject();
        tip3.addProperty("suggestion", "Stop by our wiki by visiting <wiki>");
        tip3.addProperty("word", "wiki");
        JsonObject tip4 = new JsonObject();
        tip4.addProperty("suggestion", "Please be specific if you wish to receive help! Provide us with your in-game name and the server you play on please!");
        tip4.addProperty("word", "help");
        tips.add(tip1);
        tips.add(tip2);
        tips.add(tip3);
        tips.add(tip4);
        jo.add("tips", tips);
        jo.add("swearWords", new JsonArray());
        return jo;
    }

    public JsonObject writeNotFoundDefaults(JsonObject config, JsonObject configOptions) {
        JsonObject finished = new JsonObject();
        for (Map.Entry<String, JsonElement> entrySet : configOptions.entrySet()) {
            if (!config.has(entrySet.getKey())) {
                finished.add(entrySet.getKey(), entrySet.getValue());
            } else {
                finished.add(entrySet.getKey(), config.get(entrySet.getKey()));
            }
        }
        return finished;
    }

    public boolean hasAllEntries(JsonObject config, JsonObject configOptions) {
        int count = 0;
        for (Map.Entry<String, JsonElement> entrySet : configOptions.entrySet()) {
            if (config.has(entrySet.getKey())) {
                count++;
            }
        }
        return (count == configOptions.size());
    }

    public <T extends JsonElement> T getConfigValue(String... keys) {
        JsonObject parent = (JsonObject) conf;
        JsonElement temp = parent.get(keys[0]);
        if (temp.isJsonArray())
            return (T) temp.getAsJsonArray();
        JsonElement object = temp;
        try {
            for (int i = 1; i < keys.length; i++) {
                temp = ((JsonObject) object).get(keys[i]);
                if (temp.isJsonArray())
                    return (T) temp.getAsJsonArray();
                if (temp.isJsonPrimitive())
                    return (T) temp.getAsJsonPrimitive();
                object = temp.getAsJsonObject();
            }
        } catch (NullPointerException e) {
            return (T) object;
        }
        return (T) object;
    }
}