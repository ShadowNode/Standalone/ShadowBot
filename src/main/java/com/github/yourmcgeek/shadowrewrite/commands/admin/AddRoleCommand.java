package com.github.yourmcgeek.shadowrewrite.commands.admin;

import com.github.yourmcgeek.shadowrewrite.EmbedTemplates;
import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Command(label = "massadd", hideInHelp = true, permission = Permission.ADMINISTRATOR, minArgs = 1)
public class AddRoleCommand {

    private ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(2);


    @Execute
    public CommandResult onExecute(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowRewrite main) {
        ArrayList<Long> memberToAdd = new ArrayList<>();
        if (main.getMainConfig().getConfigValue("debug").getAsBoolean())
            main.getLogger().info(args.toString() + "\n\n" + args.size());
        for (Member members : channel.getGuild().getMembers()) {
            if (members.getRoles().stream().map(Role::getName).noneMatch(name -> name.equalsIgnoreCase(args.get(0)))) {
                memberToAdd.add(members.getUser().getIdLong());
            }
        }
        if (main.getMainConfig().getConfigValue("debug").getAsBoolean())
            main.getLogger().info("Size: " + memberToAdd.size() + "\n\n" + memberToAdd.toString());
        channel.sendMessage(EmbedTemplates.SUCCESS.getEmbed().setDescription("Scheduled adding role " + args.get(0) + " to " + memberToAdd.size()).build()).queue();


        scheduledExecutorService.scheduleWithFixedDelay(() -> {

            try {
                if (memberToAdd.size() == 1) {
                    scheduledExecutorService.shutdown();
                }
//                channel.getGuild().getController().modifyMemberRoles(channel.getGuild().getMemberById(memberToAdd.get(x)), channel.getGuild().getRolesByName(args.get(0), true), null).queue();
                channel.getGuild().getController().addSingleRoleToMember(channel.getGuild().getMemberById(memberToAdd.get(0)), channel.getGuild().getRoleById(args.get(0))).queue();
                memberToAdd.remove(0);
                if (main.getMainConfig().getConfigValue("debug").getAsBoolean())
                    main.getLogger().info("Size: " + memberToAdd.size() + "\n\n" + memberToAdd.toString());
            } catch (Exception e) {
                main.getLogger().error("", e);
            }
        }, 0, 1, TimeUnit.SECONDS);
        return CommandResult.success();
    }

}
