package com.github.yourmcgeek.shadowrewrite.commands.support;

import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import me.bhop.bjdautilities.EditableMessage;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.util.List;

@Command(label = {"supportsetup", "setupsupport"}, usage = "setupsupport", description = "Provides information that needs added to the configuration file for support to work properly", permission = Permission.ADMINISTRATOR, hideInHelp = true)
public class SupportSetup {

    @Execute
    public CommandResult onExecute(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowRewrite main) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(new Color(main.getMainConfig().getConfigValue("Red").getAsInt(), main.getMainConfig().getConfigValue("Blue").getAsInt(), main.getMainConfig().getConfigValue("Green").getAsInt()));
        builder.setDescription("To make a support ticket please type your message in this channel that describes your issue fully.\n" +
                "\n" +
                "Remember to fill out the following information after creating your ticket:\n" +
                "```/username <Minecraft Username>\n" +
                "/server <Server Name>```\n" +
                "\n" +
                "Press \u2705 to start deletion of your ticket when complete.\n" +
                "Press \uD83D\uDD12 to lock your ticket to just yourself and staff members.\n" +
                "\n" +
                "When you do this a dedicated channel will be created for your issue. Once this is done Staff will contact you to try and resolve your issue.\n" +
                "\n" +
                "Tickets are not to be used for small questions that can be easily answered for these please use the #support channel. Anything that requires in-depth staff interaction warrants creating a ticket. DO NOT abuse our ticket system doing so will result in a mute, kick or ban from our Discord.");
        EditableMessage editableMessage = main.getMessenger().sendEmbed(channel, builder.build(), 0);
        Message message2 = editableMessage;
        message2.pin().queue();
        channel.getManager().setTopic("Read the pinned message to learn how to get support!").queue();
        EmbedBuilder pmBuilder = new EmbedBuilder()
                .setColor(Color.decode(main.getMainConfig().getConfigValue("colour").getAsString()))
                .addField("SupportId", channel.getId(), true)
                .addField("SupportCategoryId", message.getCategory().getId(), true)
                .addField("GuildID", String.valueOf(channel.getGuild().getIdLong()), true)
                .setDescription("Add these values in their corresponding places in your config file. Save the config," +
                        " then restart the bot!");
        try {
            message.getAuthor().openPrivateChannel().complete().sendMessage(pmBuilder.build()).queue();
        } catch (NullPointerException e) {
            main.getLogger().error("", e);
        }
        return CommandResult.success();
    }
}