package com.github.yourmcgeek.shadowrewrite.commands.admin;

import com.github.yourmcgeek.shadowrewrite.ShadowRewrite;
import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import me.bhop.bjdautilities.ReactionMenu;
import me.bhop.bjdautilities.command.annotation.Command;
import me.bhop.bjdautilities.command.annotation.Execute;
import me.bhop.bjdautilities.command.result.CommandResult;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.io.File;
import java.util.List;
import java.util.Objects;

@Command(label = {"getlogs", "getlog"}, description = "Used to get the log files!", usage = "getlog")
public class LogCommand {

    @Execute
    public CommandResult onExecute(Member member, TextChannel channel, Message message, String label, List<String> args, ShadowRewrite main) {
        if (main.getDirectory().resolve("logs").toFile().exists()) {
            ReactionMenu.Builder reactionMenuBuilder = new ReactionMenu.Builder(main.getJDA());
            reactionMenuBuilder.onClick("\u274C", ReactionMenu::destroy);
            EmbedBuilder embed = new EmbedBuilder();
            embed.setColor(Color.CYAN).setTitle("Available Logs");
            embed.setDescription("Please select the log file you want by reacting with the matching emote!");
            char endCharacter = 'a';
            String alias = "regional_indicator_symbol_";
            for (File logFile : Objects.requireNonNull(main.getDirectory().resolve("logs").toFile().listFiles())) {
                Emoji emoji = EmojiManager.getForAlias(alias + endCharacter);
                if (!logFile.isDirectory()) {
                    embed.addField(logFile.getName() + ": ", emoji.getUnicode(), true);
                    endCharacter++;
                    reactionMenuBuilder.onClick(emoji.getUnicode(), (menu, user) -> {
                        channel.sendFile(logFile, logFile.getName(), new MessageBuilder().append("Here is your requested log file ").append(user.getAsMention()).build()).queue();
                    });
                }
            }
            reactionMenuBuilder.setEmbed(embed.build()).buildAndDisplay(channel);
        }
        return CommandResult.success();
    }
}
